import assert from "assert";
import mapInvert from "../index";

// Inverting an empty map
assert.strictEqual(mapInvert(new Map()).size, 0);

// Inverting a map with one entry
const one = new Map([[123, "foo"]]);
assert.deepStrictEqual(mapInvert(one), new Map([["foo", 123]]));

// Inverting a map with many entries
const many = new Map([
  [1, "foo"],
  [2, "bar"],
  [3, "baz"],
]);
assert.deepStrictEqual(
  mapInvert(many),
  new Map([
    ["foo", 1],
    ["bar", 2],
    ["baz", 3],
  ])
);

// Inverting doesn't change the original map
assert.deepStrictEqual(
  many,
  new Map([
    [1, "foo"],
    [2, "bar"],
    [3, "baz"],
  ])
);
