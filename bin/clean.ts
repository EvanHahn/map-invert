#!/usr/bin/env node
// This lets us remove files on all platforms. Notably, `rm` is missing on Windows.
import * as path from "path";
import * as fs from "fs";

const distPath = path.join(__dirname, "..", "dist");

fs.rmSync(distPath, { recursive: true, force: true });
