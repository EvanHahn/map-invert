# map-invert

Invert [`Map`s](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Map). Keys become values and values become keys. Returns a new `Map`.

```js
import mapInvert from "map-invert";

const myMap = new Map([
  [1, "foo"],
  [2, "bar"],
  [3, "baz"],
]);

const inverted = mapInvert(myMap);

inverted.get("baz");
// => 3
```
