export default function mapInvert<K, V>(map: Readonly<Map<K, V>>): Map<V, K> {
  const result = new Map<V, K>();
  for (const [key, value] of map) {
    result.set(value, key);
  }
  return result;
}
